<?php
    session_start();
    if (isset($_SESSION["usuario"])) {
        include("respuestas.html");
    }else if(isset($_POST["usuario"]) && isset($_POST["pswd"]) && isset($_POST["confirmar"])){
        $_SESSION["usuario"] = $_POST["usuario"];
        $_SESSION["pswd"] = $_POST["pswd"];
        $_SESSION["confirmar"] = $_POST["confirmar"];

        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["img"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

       
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
       }

        $_SESSION["img"] = $target_file;

        if($_POST["pswd"] == $_POST["confirmar"]) {
            $_SESSION["respuestas"] = true;
        } else {
            $_SESSION["respuestas"] = false;
        }
        include("respuestas.html");
    }else{
        header("location:index.php");
    }
?>