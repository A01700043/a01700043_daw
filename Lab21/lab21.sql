IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaMaterial' AND type = 'P')
                DROP PROCEDURE modificaMaterial
            GO
            
            CREATE PROCEDURE modificaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                UPDATE Materiales SET Descripcion= @udescripcion, costo = @ucosto, PorcentajeImpuesto= @uimpuesto 
				Where Clave= @uclave;
            GO
			EXECUTE modificaMaterial 1000,'eefw',250,15
IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'eliminaMaterial' AND type = 'P')
                DROP PROCEDURE eliminaMaterial
            GO
            
            CREATE PROCEDURE eliminaMaterial
                @uclave NUMERIC(5,0)
            AS
                DELETE FROM Materiales Where Clave= @uclave;
            GO
			EXECUTE eliminaMaterial 5000

IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaProyecto' AND type = 'P')
                DROP PROCEDURE creaProyecto
            GO
            
            CREATE PROCEDURE creaProyecto
                @unumero NUMERIC(5,0),
                @udenominacion VARCHAR(50)
            AS
                INSERT INTO Proyectos Values (@unumero, @udenominacion)
            GO
			
			EXECUTE creaProyecto 1000,'Fierro Parienton'
		Select *
		From Proyectos

	IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaProyecto' AND type = 'P')
                DROP PROCEDURE modificaProyecto
            GO
            
            CREATE PROCEDURE modificaProyecto
			@unumero NUMERIC(5,0),
                @udenominacion VARCHAR(50)
               
            AS
                UPDATE Proyectos SET Denominacion= @udenominacion WHERE Numero= @unumero;
            GO
			
	EXECUTE modificaProyecto 1000,'Fierro Pariente'
	
	Select *
	From Proyectos

	IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'eliminaProyecto' AND type = 'P')
                DROP PROCEDURE eliminaProyecto
            GO
            
            CREATE PROCEDURE eliminaProyecto
			@unumero NUMERIC(5,0)       
            
			AS
                DELETE FROM Proyectos WHERE Numero= @unumero;
            GO
			
	EXECUTE eliminaProyecto 1000
	Select *
	From Proyectos

	IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaProveedor' AND type = 'P')
                DROP PROCEDURE creaProveedor
            GO
            
            CREATE PROCEDURE creaProveedor
			@urfc VARCHAR(20),
			@urazonsocial VARCHAR (50)     
            
			AS
                INSERT INTO Proveedores VALUES (@urfc,@urazonsocial)
            GO
EXECUTE creaProveedor 'AS23', 'Buen Proveedor'

IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaProveedor' AND type = 'P')
                DROP PROCEDURE modificaProveedor
            GO
            
            CREATE PROCEDURE modificaProveedor
			@urfc VARCHAR(20),
			@urazonsocial VARCHAR (50)     
            
			AS
            UPDATE Proveedores SET Razonsocial= @urazonsocial Where RFC=@urfc;
            GO
	Execute modificaProveedor 'AS23', 'Buen Proveedor'

IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'eliminarProveedor' AND type = 'P')
                DROP PROCEDURE eliminarProveedor
            GO
            
            CREATE PROCEDURE eliminarProveedor
			@urfc VARCHAR(20)   
            
			AS
            DELETE FROM Proveedores Where RFC= @urfc;
            GO
	Execute eliminarPRoveedor 'AS23'

	IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaEntrega' AND type = 'P')
                DROP PROCEDURE creaEntrega
            GO
            
            CREATE PROCEDURE creaEntrega
			@uclave NUMERIC(5,0),
                @urfc VARCHAR(20),
                @unumero NUMERIC(8,2),
                @ufecha DATETIME,
				@ucantidad NUMERIC(6,2)
				  
            
			AS
                INSERT INTO Entregan VALUES (@uclave,@urfc, @unumero, GETDATE(), @ucantidad)
            GO
			
			Execute creaEntrega 1000, 'Alpha2',7000,1

			IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'modificaEntrega' AND type = 'P')
                DROP PROCEDURE modificaEntrega
            GO
            
            CREATE PROCEDURE modificaEntrega
			@uclave NUMERIC(5,0),
                @urfc VARCHAR(20),
                @unumero NUMERIC(8,2),
                @ufecha DATETIME,
				@ucantidad NUMERIC(6,2)
				  
            
			AS
                UPDATE Entregan SET Fecha=GETDATE(), Cantidad=@ucantidad 
				Where Clave=@uclave AND RFC=@urfc AND Numero = @unumero;
            GO
			
		EXECUTE modificaEntrega 1000,'Delta1', 400, 5000, 5

	SELECT *
	FROM Entregan

	IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'eliminaEntrega' AND type = 'P')
                DROP PROCEDURE eliminaEntrega
            GO
            
            CREATE PROCEDURE eliminaEntrega
			@uclave NUMERIC(5,0),
                @urfc VARCHAR(20),
                @unumero NUMERIC(8,2),
            
			AS
            DELETE FROM Entregan Where Clave=@uclave And RFC=@urfc AND Numero=@unumero
            GO

		 IF EXISTS (SELECT name FROM sysobjects 
                                       WHERE name = 'queryMaterial' AND type = 'P')
                                DROP PROCEDURE queryMaterial
                            GO
                            
                            CREATE PROCEDURE queryMaterial
                                @udescripcion VARCHAR(50),
                                @ucosto NUMERIC(8,2)
                            
                            AS
                                SELECT * FROM Materiales WHERE descripcion 
                                LIKE '%'+@udescripcion+'%' AND costo > @ucosto 
                            GO

	EXECUTE queryMaterial 'Lad',20 
