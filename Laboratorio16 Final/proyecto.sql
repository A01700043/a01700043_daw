-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2018 at 06:21 AM
-- Server version: 5.5.57-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET dateformat dmy


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `proyecto`
--

-- --------------------------------------------------------

--
-- Table structure for table `Articulos`
--
drop table Articulos
select * from Articulos
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Articulos')
CREATE TABLE Articulos (
  idArticulo numeric(2) NOT NULL PRIMARY KEY IDENTITY (1,1),
  titulo varchar(100) NOT NULL,
  fecha datetime NOT NULL,
  descripcion text NOT NULL,
)
BULK INSERT equipo03.equipo03.[Articulos] 
  FROM 'e:\wwwroot\equipo03\articulos.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `BannerE`
--
select * from BannerE
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'BannerE')
CREATE TABLE BannerE (
  rfc varchar(20) NOT NULL,
  idEvento numeric(20) NOT NULL,
  PRIMARY KEY (rfc,idEvento),
  CONSTRAINT fkBannerE_rfc FOREIGN KEY (rfc) REFERENCES Institucion(rfc), 
  CONSTRAINT fkBannerE_idEvento FOREIGN KEY (idEvento) REFERENCES Evento(idEvento)
) 
BULK INSERT equipo03.equipo03.[BannerE] 
  FROM 'e:\wwwroot\equipo03\bannerE.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Banner_cv`
--
drop table Banner_cv
select * from Banner_cv
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Banner_cv')
CREATE TABLE Banner_cv (
  idCV numeric(20) NOT NULL IDENTITY (1,1) PRIMARY KEY,
  compra_venta varchar(10) NOT NULL,
  datosContacto text NOT NULL,
  material varchar(20) NOT NULL,
  enlaceImagen varchar(50) NOT NULL,
)
BULK INSERT equipo03.equipo03.[Banner_cv] 
  FROM 'e:\wwwroot\equipo03\banner_cv.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 


-- --------------------------------------------------------

--
-- Table structure for table `Capsula`
--
drop table Capsula
select * from Capsula
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Capsula')
CREATE TABLE Capsula (
  idCapsula numeric(20) NOT NULL IDENTITY (1,1) PRIMARY KEY,
  titulo varchar(100) NOT NULL,
  descripcion text NOT NULL,
  fecha datetime NOT NULL,
  enlaceCapsula varchar(50) NOT NULL
)
BULK INSERT equipo03.equipo03.[Capsula] 
  FROM 'e:\wwwroot\equipo03\capsula.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Certificado`
--
drop table Certificado
select * from Certificado
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Certificado')
CREATE TABLE Certificado (
  idCertificado numeric(20) NOT NULL IDENTITY (1,1) PRIMARY KEY,
  rfc varchar(20) NOT NULL,
  porcentaje numeric(5) NOT NULL,
  fecha datetime NOT NULL,
  CONSTRAINT fk_rfc FOREIGN KEY (rfc) REFERENCES Institucion(rfc)
)
BULK INSERT equipo03.equipo03.[Certificado] 
  FROM 'e:\wwwroot\equipo03\certificado.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 


-- --------------------------------------------------------

--
-- Table structure for table `Comentario`
--
drop table Comentario
select * from Comentario
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Comentario')
CREATE TABLE Comentario (
  idComentario numeric(20) NOT NULL IDENTITY (1,1) PRIMARY KEY,
  nUser varchar(50) NOT NULL,
  idPost numeric(20) NOT NULL,
  fecha datetime NOT NULL,
  CONSTRAINT fkComentario_nUser FOREIGN KEY (nUser) REFERENCES Usuario(nUser), 
  CONSTRAINT fkComentario_idPost FOREIGN KEY (idPost) REFERENCES Post(idPost)
)
BULK INSERT equipo03.equipo03.[Comentario] 
  FROM 'e:\wwwroot\equipo03\comentario.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Curso`
--
drop table curso
select * from Curso
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Curso')
CREATE TABLE Curso (
  idCurso numeric(20) NOT NULL IDENTITY (1,1) PRIMARY KEY,
  titulo varchar(50) NOT NULL,
  descripcion varchar(100) NOT NULL,
  fecha datetime NOT NULL,
  enlace varchar(100) NOT NULL
)
BULK INSERT equipo03.equipo03.[Curso] 
  FROM 'e:\wwwroot\equipo03\curso.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 


-- --------------------------------------------------------

--
-- Table structure for table `Estado`
--
select * from Estado
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Estado')
CREATE TABLE Estado (
  idEstado numeric(20) NOT NULL IDENTITY (1,1) PRIMARY KEY,
  idPais numeric(20) NOT NULL,
  Nombre varchar(50) NOT NULL,
  CONSTRAINT fk_idPais FOREIGN KEY (idPais) REFERENCES Pais(idPais)
)
BULK INSERT equipo03.equipo03.[Estado] 
  FROM 'e:\wwwroot\equipo03\estado.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Evento`
--
select * from Evento
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Evento')
CREATE TABLE Evento (
  idEvento numeric(20) NOT NULL IDENTITY (1,1) PRIMARY KEY,
  nombre varchar(50) NOT NULL,
  idEstado numeric(20) NOT NULL,
  codigoPostal numeric(11) NOT NULL,
  descripcion varchar(100) NOT NULL,
  direccion varchar(150) NOT NULL,
  CONSTRAINT fkEvento_idEstado FOREIGN KEY (idEstado) REFERENCES Estado(idEstado)
) 
BULK INSERT equipo03.equipo03.[Evento] 
  FROM 'e:\wwwroot\equipo03\evento.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Institucion`
--
select * from Institucion
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Institucion')
CREATE TABLE Institucion (
  rfc varchar(20) NOT NULL PRIMARY KEY,
  razonSocial varchar(100) NOT NULL,
  idEstado numeric(20) NOT NULL,
  codigoPostal numeric(11) NOT NULL,
  enlaceLogo varchar(50) NOT NULL,
  CONSTRAINT fk_idEstado FOREIGN KEY (idEstado) REFERENCES Estado(idEstado)
) 
BULK INSERT equipo03.equipo03.[Institucion] 
  FROM 'e:\wwwroot\equipo03\institucion.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 
-- --------------------------------------------------------

--
-- Table structure for table `Institucion_Evento`
--
drop table Institucion_Evento
select * from Institucion_Evento
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Institucion_Evento')
CREATE TABLE Institucion_Evento (
  rfc varchar(20) NOT NULL,
  idEvento numeric(20) NOT NULL,
  fecha datetime NOT NULL,
  PRIMARY KEY (rfc,idEvento,fecha),
  CONSTRAINT fkInstitucionEvento_rfc FOREIGN KEY (rfc) REFERENCES Institucion(rfc),
  CONSTRAINT fkInstitucionEvento_idEvento FOREIGN KEY (idEvento) REFERENCES Evento(idEvento)
) 
BULK INSERT equipo03.equipo03.[Institucion_Evento] 
  FROM 'e:\wwwroot\equipo03\institucion_evento.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Institucion_Material`
--
drop table Institucion_Material
select * from Institucion_Material
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Institucion_Material')
CREATE TABLE Institucion_Material (
  rfc varchar(20) NOT NULL,
  idMaterial numeric(20) NOT NULL,
  fecha datetime NOT NULL,
  cantidad decimal (20,3) NOT NULL,
  PRIMARY KEY (rfc,idMaterial,fecha),
  CONSTRAINT fkInstitucionMaterial_rfc FOREIGN KEY (rfc) REFERENCES Institucion(rfc),
  CONSTRAINT fkInstitucionMaterial_idMaterial FOREIGN KEY (idMaterial) REFERENCES Material(idMaterial)
) 
BULK INSERT equipo03.equipo03.[Institucion_Material] 
  FROM 'e:\wwwroot\equipo03\institucion_material.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Material`
--
select * from Material
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Material')
CREATE TABLE Material (
  idMaterial numeric(20) NOT NULL IDENTITY(1,1) PRIMARY KEY,
  nombre varchar(15) NOT NULL,
  descripcion text NOT NULL
) 
BULK INSERT equipo03.equipo03.[Material] 
  FROM 'e:\wwwroot\equipo03\material.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Pais`
--
select * from Pais
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Pais')
CREATE TABLE Pais (
  idPais numeric(20) NOT NULL IDENTITY(1,1) PRIMARY KEY,
  nombre varchar(50) NOT NULL
) 
BULK INSERT equipo03.equipo03.[Pais] 
  FROM 'e:\wwwroot\equipo03\pais.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 
-- --------------------------------------------------------

--
-- Table structure for table `Post`
--
drop table Post
select * from Post
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Post')
CREATE TABLE Post (
  idPost numeric(20) NOT NULL IDENTITY(1,1) PRIMARY KEY,
  nUser varchar(50) NOT NULL,
  fechaInicial datetime NOT NULL,
  fechaUpdate datetime NOT NULL,
  tema varchar(100) NOT NULL,
  descripcion text NOT NULL,
  enlace varchar(50) NOT NULL,
  CONSTRAINT fk_nUser FOREIGN KEY (nUser) REFERENCES Usuario(nUser)
) 
BULK INSERT equipo03.equipo03.[Post] 
  FROM 'e:\wwwroot\equipo03\post.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 
-- --------------------------------------------------------

--
-- Table structure for table `Privilegios`
--
select * from Privilegios
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Privilegios')
CREATE TABLE Privilegios (
  idPrivilegios numeric(20) NOT NULL IDENTITY(1,1) PRIMARY KEY,
  nombre varchar(100) NOT NULL,
  descripcion text NOT NULL
) 
BULK INSERT equipo03.equipo03.[Privilegios] 
  FROM 'e:\wwwroot\equipo03\privilegios.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 
-- --------------------------------------------------------

--
-- Table structure for table `Rol`
--
select * from Rol
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Rol')
CREATE TABLE Rol (
  idRol numeric(20) NOT NULL IDENTITY(1,1) PRIMARY KEY,
  nombre varchar(50) NOT NULL
)
BULK INSERT equipo03.equipo03.[Rol] 
  FROM 'e:\wwwroot\equipo03\rol.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Rol_Privilegios`
--
drop table Rol_Privilegios
select * from Rol_Privilegios
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Rol_Privilegios')
CREATE TABLE Rol_Privilegios (
  idRol numeric(20) NOT NULL,
  idPrivilegios numeric(20) NOT NULL,
  PRIMARY KEY (idRol,idPrivilegios),
  CONSTRAINT fk_idRol FOREIGN KEY (idRol) REFERENCES Rol(idRol),
  CONSTRAINT fk_idPrivilegios FOREIGN KEY (idPrivilegios) REFERENCES Privilegios(idPrivilegios)
)
BULK INSERT equipo03.equipo03.[Rol_Privilegios] 
  FROM 'e:\wwwroot\equipo03\rol_privilegios.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Usuario`
--
select * from Usuario
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Usuario')
CREATE TABLE Usuario (
  nUser varchar(50) NOT NULL PRIMARY KEY,
  password varchar(40) NOT NULL
) 
BULK INSERT equipo03.equipo03.[Usuario] 
  FROM 'e:\wwwroot\equipo03\usuario.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

-- --------------------------------------------------------

--
-- Table structure for table `Usuario_Institucion`
--
drop table Usuario_Institucion
select * from Usuario_Institucion
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Usuario_Institucion')
CREATE TABLE Usuario_Institucion (
  nUser varchar(50) NOT NULL,
  rfc varchar(20) NOT NULL,
  fecha datetime NOT NULL,
  PRIMARY KEY (nUser,rfc),
 CONSTRAINT fkUsuarioInstitucion_nUser FOREIGN KEY (nUser) REFERENCES Usuario(nUser),
 CONSTRAINT fkUsuarioInstitucion_rfc FOREIGN KEY (rfc) REFERENCES Institucion(rfc)
) 
BULK INSERT equipo03.equipo03.[Usuario_Institucion] 
  FROM 'e:\wwwroot\equipo03\usuario_institucion.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 
-- --------------------------------------------------------

--
-- Table structure for table `Usuario_Rol`
--
drop table Usuario_Rol
select * from Usuario_Rol
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Usuario_Rol')
CREATE TABLE Usuario_Rol (
  nUser varchar(50) NOT NULL,
  idRol numeric(20) NOT NULL,
  fecha datetime NOT NULL,
  PRIMARY KEY (nUser,idRol),
 CONSTRAINT fkUsuarioRol_nUser FOREIGN KEY (nUser) REFERENCES Usuario(nUser),
 CONSTRAINT fkUsuarioRol_idRol FOREIGN KEY (idRol) REFERENCES Rol(idRol)
)
BULK INSERT equipo03.equipo03.[Usuario_Rol] 
  FROM 'e:\wwwroot\equipo03\usuario_rol.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


