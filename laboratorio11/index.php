<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Laboratorio 11 - A01700043</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.min.css" rel="stylesheet">

  </head>

    <!-- Header -->
    <header class="masthead d-flex">
      <div class="container text-center my-auto">
        <h1 class="mb-1">Laboratorio 11</h1>
        <h3 class="mb-5">
          <em>Cristhian M. Quiroz A01700043</em>
        </h3>
        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Cuestionario y Validacion</a>
      </div>
      <div class="overlay"></div>
    </header>

    <!-- About -->
    <section class="content-section bg-light" id="about">
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2>Cuestionario:</h2>
            <dt><strong>¿Por qué es una buena práctica separar el controlador de la vista?.</strong></dt>
			<dd>Debido a que forma parte de un patrón de diseño, mediante el cual el usuario no puede acceder a información que no es de su incumbencia o que podría resultar vulnerable<br><br></dd>
			<dt><strong>Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.</strong></dt>
			<dd><p>_wakeup() : Reestablecer conexión con las bases de datos</p> <p>htmlspecialchars() traductor de caracteres especiales a entidades</p><br><br></dd>
			<dt><strong>¿Qué es XSS y cómo se puede prevenir?</strong></dt>
			<dd>Cross Site Scripting. Se basa en que un usuario puede agregar codigos scripts y hacer que estos se ejecuten, por lo cual cuando exista alguna forma es necesario que esto se tome como html y no como un script<br><br></dd>
			<dt><strong>Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?</strong></dt>
			<dd><p>$_Session Utilizado para las sesiones</p> <p>$_Server Utilizado Para El Servidor</p><br><br><br><br></dd>
			</dl>
          </div>
        </div>
      </div>
    </section>
    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <ul class="list-inline mb-5">
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-twitter"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white" href="#">
              <i class="icon-social-github"></i>
            </a>
          </li>
        </ul>
        <p class="text-muted small mb-0">Copyright &copy; Your Website 2017</p>
<?php
// define variables and set to empty values
$name = $email = $gender = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = test_input($_POST["name"]);
  $email = test_input($_POST["email"]);
  $gender = test_input($_POST["gender"]);
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
   
}
?>

<h2>Validacion Del Laboratorio 11</h2>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Nombre: <input type="text" name="name">
  <br><br>
  Correo Electronico: <input type="text" name="email">

  <br><br>
  Sexo:
  <input type="radio" name="gender" value="Femenino">Femenino
  <input type="radio" name="gender" value="Masculino">Masculino

  <br><br>
  <input type="submit" name="submit" value="Enviar">  
</form>

<?php
echo "<h2>Your Input:</h2>";
echo $name;
echo "<br>";
echo $email;
echo "<br>";
echo $gender;
?>
      </div>
    </footer>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>

  </body>

</html>
