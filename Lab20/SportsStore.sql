-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar, 2018 at 01:18 AM
-- Server version: 10.1.26-MrsDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `SportsStore`
--

-- --------------------------------------------------------

--
-- Table structure for table `Kit`
--

CREATE TABLE `Kit` (
  `id` int(4) NOT NULL,
  `color` varchar(40) NOT NULL,
  `price` int(10) NOT NULL,
  `club` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Kit`
--

INSERT INTO `Kit` (`id`, `color`, `price`, `club`) VALUES
(1, 'Pink', 95, 'Juventus'),
(2, 'Red', 120, 'Manchester United'),
(3, 'Black', 125, 'Real Madrid'),
(4, 'Green', 95, 'Celtic'),
(5, 'Blue', 85, 'Chelsea'),
(6, 'White', 75, 'LA Galaxy'),
(7, 'White', 85, 'Juventus'),
(8, 'Yellow', 120, 'Dortmunt'),
(9, 'Blue', 125, 'America FC');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Kit`
--
ALTER TABLE `Kit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Kit`
--
ALTER TABLE `Kit`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
