BULK INSERT a1700043.a1700043.[Proveedores] 
  FROM 'e:\wwwroot\a1700043\proveedores.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 
  SELECT  * FROM Proveedores