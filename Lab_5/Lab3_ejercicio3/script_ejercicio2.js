
function tienda(z) {
	var seleccion=(this.pregunta2.value);
	var pregunta;
	var respuesta;
	switch (seleccion) {
    case "1":
        pregunta = "¿Por qué es una buena práctica usar JavaScript para checar que sean válidos los inputs de las formas antes de enviar los datos al servidor?";
		respuesta = "Para verificar que no existan errores comunes que pueden ser facilmente identificados, antes de que los datos pasen al servidor";
		break;
    case "2":
         pregunta = "¿Cómo puedes saltarte la seguridad de validaciones hechas con JavaScript?";
		 respuesta = "Desactivando el uso de JavaScript dentro de la configuración del navegador";
		 break;
    case "3":
         pregunta = "Si te puedes saltar la seguridad de las validaciones de JavaScript, entonces ¿por qué la primera pregunta dice que es una buena práctica?";
		 respuesta = "Porque evita doble trabajo para el servidor al verificar.Si hay errores que pueden ser identificados de manera previa, esto agiliza el proceso para obtener la información, sin tener que hacer una doble consulta ";
		 break;
    default:
         pregunta = "No existe la pregunta especificada";
		 respuesta = "";
		 break;
	}
	var pr=document.getElementById("pregunta");
	pr.innerText=pregunta;
	var rp=document.getElementById("respuesta");
	rp.innerText=respuesta;
    z.preventDefault();
}
var store1=document.getElementById("shop");
store1.addEventListener("submit", tienda, true);